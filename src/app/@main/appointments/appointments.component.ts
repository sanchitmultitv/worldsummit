import { Component, OnInit } from '@angular/core';
import { DataService, localService } from 'src/app/services/data.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {

  user_profile;
  callLists:any=[];
  callListattendee:any=[];
  constructor(private _ds:DataService,private _ls:localService) { }

  ngOnInit(): void {
    this.user_profile = JSON.parse(localStorage.getItem('virtual'));
    this.getScheduleCall();
    // this.getScheduleCallatttendee();
  }
  getScheduleCall(){
    this._ds.getScheduleList(this.user_profile.id).subscribe((res:any)=>{
      console.log(res);
      this.callLists = res.result;
      console.log(this.callLists);

    })
  }
  analytics(){
    this._ls.stepUpAnalytics('click_video_meeting');
  }
  // getScheduleCallatttendee(){
  //   this._ds.getScheduleListatttendee(this.user_profile.id).subscribe(res=>{
  //     console.log(res);
  //     this.callListattendee = res.result;
  //   })
  // }

}
