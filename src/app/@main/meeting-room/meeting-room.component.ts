import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { BreakoutComponent } from "../breakout/breakout.component";
import { Router } from "@angular/router";

@Component({
  selector: "app-meeting-room",
  templateUrl: "./meeting-room.component.html",
  styleUrls: ["./meeting-room.component.scss"],
})
export class MeetingRoomComponent implements OnInit {
  // @Input() a:any;

  constructor(public router: Router) {}
  // @ViewChild(BreakoutComponent) 'xyz': BreakoutComponent;

  ngOnInit(): void {}

  gotoMeeting(a) {
    // alert(a)
    // this.xyz.gotoRoom(a);
    if (a == "ibm") {
      this.router.navigate(["/breakout"]);
    }
    if (a == "hexagon") {
      this.router.navigate(["/breakout2"]);
    }
    if (a == "gadgeon") {
      this.router.navigate(["/breakout3"]);
    }
    if (a == "openroom") {
      this.router.navigate(["/breakout4"]);
    }
  }
}
