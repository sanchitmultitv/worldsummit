import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentsComponent } from './appointments/appointments.component';
import { BreakoutComponent } from './breakout/breakout.component';
import { LayoutComponent } from './layout.component';
import { MeetingRoomComponent } from './meeting-room/meeting-room.component';
import { Room2Component } from './room2/room2.component';
import { Room3Component } from './room3/room3.component';
import { Room4Component } from './room4/room4.component';

const routes: Routes = [
  { path:'', redirectTo:'lobby', pathMatch:'full' },
  { path: 'lobby', loadChildren: ()=>import('../@core/lobby/lobby.module').then(m=>m.LobbyModule)},
  { path: 'auditorium', loadChildren: ()=>import('../@core/auditorium/auditorium.module').then(m=>m.AuditoriumModule) },
  { path: 'exhibition', loadChildren: ()=>import('../@core/exhibition-hall/exhibition-hall.module').then(m=>m.ExhibitionHallModule) },
  { path: 'lounge', loadChildren: ()=>import('../@core/networking-lounge/networking-lounge.module').then(m=>m.NetworkingLoungeModule) },
  { path: 'gamezone', loadChildren: ()=>import('../@core/game-zone/game-zone.module').then(m=>m.GameZoneModule) },
  { path: 'photobooth', loadChildren: ()=>import('../@core/photobooth/photobooth.module').then(m=>m.PhotoboothModule) },
  { path: 'helpdesk', loadChildren: ()=>import('../@core/helpdesk/helpdesk.module').then(m=>m.HelpdeskModule) },
  { path: 'signaturewall', loadChildren: ()=>import('../@core/signaturewall/signaturewall.module').then(m=>m.SignaturewallModule) },
  { path: 'appointments', component:AppointmentsComponent },
  { path: 'breakout', component:BreakoutComponent },
  { path: 'breakout2', component:Room2Component },
  { path: 'breakout3', component:Room3Component },
  { path: 'breakout4', component:Room4Component },
  { path: 'rooms', component:MeetingRoomComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
