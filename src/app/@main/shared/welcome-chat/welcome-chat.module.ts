import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WelcomeChatComponent } from './welcome-chat.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [WelcomeChatComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[WelcomeChatComponent]
})
export class WelcomeChatModule { }
