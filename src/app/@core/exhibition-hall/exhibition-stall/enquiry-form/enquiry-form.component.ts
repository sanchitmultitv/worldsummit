import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
@Component({
  selector: 'app-enquiry-form',
  templateUrl: './enquiry-form.component.html',
  styleUrls: ['./enquiry-form.component.scss']
})
export class EnquiryFormComponent implements OnInit {
  enquiryForm:FormGroup;
  items: string[] = ['fire safety', 'security solutions', 'electrical products', 'building management system'];

  constructor(private fb:FormBuilder, private _ds: DataService) { }

  ngOnInit(): void {
    this.enquiryForm =  this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      company: ['', Validators.required],
      contact: ['', Validators.required],
      // category: this.fb.array([]),
      query: ['', Validators.required]
    });
    // this.items.forEach(item => {
    //   this.addDynamicElement.push(this.fb.control(''));
    // });
  }
  // get addDynamicElement() {
  //   return this.enquiryForm.get('category') as FormArray
  // }
  submitEnquiryForm(){
    console.log(this.enquiryForm.value)
    const formData = new FormData();
    formData.append('name', this.enquiryForm.value.name);
    formData.append('email', this.enquiryForm.value.email);
    formData.append('company', this.enquiryForm.value.company);
    formData.append('contact', this.enquiryForm.value.contact);
    formData.append('category', this.enquiryForm.value.query);
    this._ds.submitEnquiryForm(formData).subscribe(res=>console.log(res));
    this.enquiryForm.reset();
    $('#enquiry_modal').modal('hide');

  }
  closeModal(){
    $('#enquiry_modal').modal('hide');
  }
  ischecking(event){
    const checkedEv:any = document.querySelector('#'+event+':checked');
    console.log(event, checkedEv)
    if((checkedEv!=null)||(checkedEv!=undefined)||(checkedEv!='')){

      // this.items.forEach(e=>{
      // });
    }
  }
}
